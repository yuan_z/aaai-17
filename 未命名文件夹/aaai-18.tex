\def\year{2018}\relax
%File: formatting-instruction.tex
\documentclass[letterpaper]{article} %DO NOT CHANGE THIS
\usepackage{aaai18}  %Required
\usepackage{times}  %Required
\usepackage{helvet}  %Required
\usepackage{courier}  %Required
\usepackage{url}  %Required
\usepackage{graphicx}  %Required
\frenchspacing  %Required
\setlength{\pdfpagewidth}{8.5in}  %Required
\setlength{\pdfpageheight}{11in}  %Required


\usepackage{times}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{amsfonts}
\usepackage{amsmath}  
\usepackage{amssymb}
\usepackage{caption}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{subfigure}
\usepackage{multirow}
\renewcommand{\algorithmicrequire}{\textbf{Input:}} 
\renewcommand{\algorithmicensure}{\textbf{Output:}} 
\usetikzlibrary{bayesnet}


%PDF Info Is Required:
  \pdfinfo{
/Title (COSINE: Community-Preserving Social Network Embedding from Information Diffusion Cascades)
/Author (Yuan Zhang, Tianshu Lyu, Yan Zhang)}
\setcounter{secnumdepth}{0}  
 \begin{document}
% The file aaai.sty is the style file for AAAI Press 
% proceedings, working notes, and technical reports.
%
\title{COSINE: Community-Preserving Social Network Embedding \\from Information Diffusion Cascades}
\author{Yuan Zhang, Tianshu Lyu, Yan Zhang\\
Key Laboratory of Machine Perception (MOE)\\
Department of Machine Intelligence\\
Peking University, Beijing, China\\
\{yuan.z, lyutianshu\}@pku.edu.cn, zhy@cis.pku.edu.cn
}
\maketitle
\begin{abstract}
This paper studies the problem of social network embedding without relying on network structures that are usually not observed in many cases. We address that the information diffusion process across networks naturally reflects rich proximity relationships between users. Meanwhile, social networks contain multiple \textit{communities} regularizing communication pathways for information propagation.
Based on the above observations, we propose a probabilistic generative model, called COSINE, to learn community-preserving social network embeddings from the recurrent and time-stamped social contagion logs, namely \textit{information diffusion cascades}. The learned embeddings therefore capture the \textit{high-order user proximities} in social networks. Leveraging COSINE, we are able to discover underlying social communities and predict temporal dynamics of social contagion. Experimental results on both synthetic and real-world datasets show that our proposed model significantly outperforms the existing approaches. 
\end{abstract}

\section{Introduction}
Network analysis has become an increasingly popular research area in the past decades with the emergence of online social media and social network sites. Network embedding is a fundamental problem in this area, aiming to learn a continuous representation of networks that can be used as input features for such downstream applications as visualization, node classification and link prediction~\cite{tang2015line}. 

Recent advances start from DeepWalk~\cite{perozzi2014deepwalk} which uses random walks to generate node sequences as ``sentences'' and then exploits word2vec~\cite{mikolov2013distributed} developed in language modeling to obtain network embeddings. Tang et. al.~\shortcite{tang2015line} argue that DeepWalk only captures the \textit{second-order proximity} and further incorporate the \textit{first-order proximity} into their proposed model LINE. Grover and Leskovec~\shortcite{grover2016node2vec} extend DeepWalk in another aspect, suggesting that a second-order random walk approach generates better learning samples. All of these approaches require prior knowledge of network structures. In many cases, however, we only observe when a node propagates information or becomes infected (e.g., retweeting and clicking like) but the structural connectivity is unknown~\cite{gomez11netrate}. Besides, network structures, sometimes even when observed, only provide partial information about social dynamics. For instance, we cannot tell the interaction intensity between two users only from a who-follow-who network; a user might seldom contact some of her followees while interacting actively with others.

Bourigault et. al.~\shortcite{bourigault2014learning} first propose to learn social network embeddings from information diffusion cascades instead of network structures via a learning-to-rank approach. 
Later work~\cite{kurashima2014probabilistic,bourigault2016representation} attempts to preserve temporal dynamics of information diffusion in social embeddings. 
Although they raise meaningful problems and provide inspiring preliminary solutions, most existing work fails to consider a very important notion in social networks, \textit{communities}, meaning tightly knitted user clusters~\cite{Girvan2002Community}. 
The lack of consideration on communities may render it difficult to accurately deal with noisy social interaction logs involving highly volatile user behaviors~\cite{Hu2015Community}. 

More importantly, community structures in networks actually reflect the \textit{high-order proximities}. Users in a same community often share similar opinions and behaviors due to fast social contagion within that community, and should therefore be considered to have close relationships. 

In this paper, we propose a probabilistic generative model, COSINE (COmmunity-preserving SocIal Network Embeddings), to learn social network representations directly from more easily observed information diffusion cascades than network structures. 
As shown in Section 2, the nature of the diffusion process along with community structure regularization allows our approach to exploit not only the first-order and second-order proximities, but also, more generally, the high-order proximities between users.
Leveraging COSINE, we are able to discover underlying social communities and predict temporal dynamics of social contagion.

Our approach jointly conducts metric learning (e.g., NetInf~\cite{gomez2010inferring}, NetRate~\cite{gomez11netrate}) and multidimensional scaling (e.g., classic MDS~\cite{cox2001multidimensional}, LLE~\cite{Roweis2000Nonlinear}). Zhou et. al.~\shortcite{Zhou2013Learning} exploit $l_1$ and nuclear norm regularizations to avoid over-fitting issues when estimating metric matrices (i.e., social influence matrices) without prior information of network structures. In fact, we find that geometric constraints and clustering properties, associated with community structures, in the representation space can naturally provide sparsity and low-rank regularizations without additional complicated computations, respectively. The mutual reinforcement enables our model to outperform its pipelined variant carrying out these two procedures separately.

%\textbf[rev.....] ---------------------
In summary, we make the following main contributions:
\begin{itemize}
  \item We propose a novel generative model, called COSINE, for social network embedding from cascade data. To the best of our knowledge, COSINE is the first to exploit the information diffusion process regularized by community structures to preserve rich types of proximity relationships in social networks.

  %COSINE reflects rich proximity relationships between users by simultaneously modeling long-tailed temporal dynamics of the information diffusion process and preserving community structures in the embedding space. 
  \item We also design an efficient inference algorithm that guarantees linear scalability w.r.t. increasing input data. 
  \item COSINE is evaluated with extensive experiments on both synthetic and real-world datasets. Results demonstrate its substantial performance improvement, robustness with limited training data, and application in understanding social contagion and interaction relationships in social networks. 
  %Our case study also demonstrates its application in understanding social contagion and interaction relationships in social networks.
  %and proves scalability of our proposed approach.
\end{itemize}



\section{Background: Information Diffusion Process}
Our approach is built upon the continuous-time diffusion model for cascade data in social networks as in~\cite{gomez11netrate,Du2013Scalable}. Compared with its discrete counterparts~\cite{kempe2003maximizing} where information propagates iteratively in rounds, the continuous-time model is more appropriate in reality.

We assume that the diffusion process occurs over an unobserved underlying network $\cal G=(V,E)$. The process begins from a source user (node) $u$ at time zero. The contagion is transmitted from the source user to its direct neighbors. Once infected, users continue to transmit the contagion to their respective neighbors. Each transmission through an edge, say $(u,v)$, entails an independent random spreading time $\tau$, we call in this paper the \textit{local transmission time}, sampled from a distribution over time $f_{uv}(\tau)$. We assume a user cannot get infected twice, and thus the \textit{global transmission time} $t_{uv}$ for a user $v$ to get infected is the earliest infection time over all possible paths from the source user $u$ to the user $v$,
\begin{equation} 
t_{uv} = \min_{p\in \mathcal{P}_{uv}}\sum_{(i,j)\in p} \tau_{ij},
\end{equation}
where $\mathcal{P}_{uv}$ denotes the set of all paths from $u$ to $v$.

\begin{figure}
\includegraphics[scale = 0.55]{diffusion}
\caption{An illustrative example of information diffusion. User 0 is the source user. The numbers near other nodes indicate the expected global transmission time intervals where local transmission time intervals over all edges are sampled from the same distribution, $Exponential(1.0)$. We note that the information diffusion process regularized by community structures reflects rich proximity relationships between users.}
\label{fig:diffusion}
\end{figure}

Note that the diffusion process are not only determined by friendship-link distances (first-order proximity), but also relevant to the overall network structure due to group effects. In other words, even when user $u$ and user $v$ are not close neighbors, the information can still be quickly transmitted to $v$ if they share similar neighbors (second-order proximity), or more generally, if they are in the same interaction-intensive \textit{communities} (high-order proximities). For example, in Figure~\ref{fig:diffusion}, the contagion originated from user $0$ spreads to user $5$ almost as fast as to its direct neighbor user $1$ because they share common neighbors (i.e., user $3$ and user $4$); user 9 is expected to receive the contagion earlier than user $2$ thanks to the densely connected community, although two-hops farther away from the source. 

%the expected global transmission time for user 5 is almost the same as that for user 1 as user 5 share common neighbors (i.e., user 3 and user 4) with the source; the expected global transmission time for user 9 is less than that for user 2, although user 9 is two hops farther away from the source, as . 
% 

This property naturally enables our diffusion-based approach to learn embeddings that capture the \textit{high-order proximities} between users in social networks.


% [0. a short intro to diffusion process] [what is global transmission time interval]

% [1. fails to capture social activeness, each node sums to one; counterexample, nodes with few links have larger probability]

% [2. natural higher-order proximity with appropriate distributions compared to LINE]

%[therefore, our goal is to embedding to preserve diffusion structures]
\section{Proposed Model: COSINE}
\subsection{Model Framework}
Figure~\ref{graph} shows the probabilistic graphical representation of our model. The input is the diffusion cascades $\mathcal C = \bigcup_{u=1,...,N}\mathcal C_{u}$, where $N$ is the number of users and $\mathcal C_u$ denotes the diffusion cascades originated from user $u$. Each cascade $c$ includes a set of tuples, namely $\{(v, t_v^c)\}_{v \in \mathcal U_{c}}$, of infected users $\mathcal U_{c}$ and their infection timestamps (global transmission time) within an observation time window $(0,T_c]$. Here, we reset the ``clock'' to 0 at the start of each cascade and assume $T_c = T$ for each cascade $c$ for simplicity. 

Our goal is to learn a low-dimensional continuous representation $x_u \in \mathbb{R}^{D}$ for each user $u$, where $D$ denotes the number of dimensions. Each dimension may act as a subset of social interaction channels. We use the squared Euclidean distances\footnote{We have tested several dissimilarity measures and this one offers a good compromise.} (i.e., $d_{uv} = \Vert x_u - x_v\Vert^2$) in the low-dimensional space to represent mean values of the global transmission time distributions from which cascading timestamps are drawn. Thus, our approach can jointly model temporal dynamics of information diffusion and learn community-preserving embeddings ``with mutual benefits''.

%and distances in the low-dimensional space can represent proper social relations between users.
%  
\begin{figure}
\centering
\begin{tikzpicture}

  % Define nodes
  \node[latent] (x){$x_{u}$};
  \node[latent,dashed, right = of x] (xd){$x_{v}$};
  \node[obs,below=of x,xshift = -0.1cm]  (tobs) {$t^c_v$};
  \node[latent,right=of tobs,xshift = 0.2cm]  (tmis) {$\overline{t^c_v}$};
  \node[latent,above=of x]  (z) {$z_u$};
  \node[latent,left=of z]  (gamma) {$\gamma_k$};
  \node[latent,left=of x, yshift = 0.5cm] (mu) {$\mu_k$};
  \node[latent,left=of x, yshift = -0.5cm] (sigma) {$\Sigma_k$};
  % Connect the nodes
  \edge {mu,sigma,z} {x} ; 
  \edge {gamma} {z} ;
  \edge {x,xd} {tobs};
  \edge {x,xd} {tmis};
  % Plates
  \plate {t} {(tobs)} {$v\in \mathcal U_c$};
  \plate {tt} {(tmis)} {$v\in\overline{\mathcal U_c}$};
  \plate {c} {(t)(tt)} {$c\in \mathcal C_u$}
  \plate {} {(t)(tt)(z)(x)(tobs)(tmis)(c)} {$N$};
  \plate {} {(mu)(sigma)(gamma)} {$K$};

  % \plate {} {(w)(y)(yx.north west)(yx.south west)} {$M$} ;

\end{tikzpicture}
\caption{Probabilistic graphical representation of our model.}
\label{graph}
\end{figure}


\subsubsection{Modeling Temporal Dynamics of Information Diffusion with Long-Tailed Distributions.}

Information diffusion process reflects rich proximity relationships between users. However, data sparsity and volatile user behaviors make it difficult to model temporal dynamics of information propagation. Besides, we can only observe the ``early stages'' of the information diffusion processes within an observation window $T$. Normal distributions might be easily biased towards longer transmission time and sensitive to missing data (an ``infected'' user would be considered as ``uninfected'' in a cascade if her record were missing). 

Therefore, we propose to exploit a long-tailed distribution, inverse-Gaussian distribution (a.k.a. Wald distribution) that is often used in survival data analysis, to model the sparse infection timestamps with large uncertainty. We refer the reader to Model Inference for more intuitions.

Specifically, supposing user $u$ is the source in a cascade $c$, we let the squared Euclidean distance $d_{uv}=\Vert x_u - x_v\Vert^2$ in the representation space denote both the mean and variance parameters of the global transmission time distribution for user $v$, i.e.,
\begin{equation} 
f(t_{v}^{c}) = \frac{d_{uv}}{\sqrt{2\pi}(t_{v}^{c})^{3/2}} \exp\{-\frac{1}{2}\cdot\frac{(t_{v}^{c}-d_{uv})^2}{t_{v}^{c}}\}.
\end{equation}
Meanwhile, we regard timestamps $\{\overline{t_{v}^{c}}\}$ of those ``survival'' users who are not involved in a given cascade as missing data where the missing mechanism is $\mathbb I(\overline{t_{v}^{c}} >T)$ and use statistical methods to impute those missing timestamps. Compared with those leveraging alternative survival analysis methods (e.g.,~\cite{kurashima2014probabilistic,bourigault2016representation}), our approach is more efficient and scalable, especially when the lengths of cascades are relatively large.

\subsubsection{Preserving Community Structure.}
There exist multiple dense subgraphs, communities, in social networks with close connection to information contagion. In our approach, we use the Gaussian mixture model (GMM) as priors on users' embeddings to preserve community structure in the given network. We assume that there are $K$ communities, and that each user $u$ belongs to exactly one community denoted by $z_u \in [1,...,K]$ drawn from a multinomial distribution $\{\gamma_k\}$. Each community $k$ is represented by a Gaussian component with mean $\mu_k$ and covariance matrix $\Sigma_k$ that reflect the community's relative position and connectivity density, respectively.

Our model can be extended to capture overlapping communities with an unknown number of communities by changing GMM to hierarchical mixture models with Dirichlet priors (e.g., the one proposed in~\cite{10.2307/27639773}) with little effort. Yet, this paper only focuses on the simple case for clarity.


\begin{table}[t]
\begin{center}
\centering
\caption{Notations used in our paper.}
\label{tab}
\begin{tabular}{r|p{178pt}}
	\hline
	 Symbols & Descriptions \\
	\hline
	 N, K, D & numbers of users, communities, dimensions\\
	 T & observation time window\\
	 $\mathcal{C}, \mathcal{C}_u$& sets of diffusion cascades and those originated from user $u$\\
	 $\mathcal{U}_c$, $\overline{\mathcal{U}_c}$& sets of infected and uninfected users in cascade $c$ \\
	 $z_u$ & latent community membership of user $u$\\
	 $x_u$ & embedding vector of user $u$\\
	 $d_{uv}$ & squared Euclidean distance between embedding vectors of user $u$ and $v$, i.e., $\Vert x_u - x_v\Vert^2$ \\
	 $t_u^{c}, \bar{t_u^{c}}, \widetilde{t_u^{c}}$ & observed, unobserved, imputed timestamps of user $u$ getting infected in cascade $c$\\
	 $\gamma_k$ & prior probability / mixing weight of cluster $k$\\
	 $r_{uk}$ & conditional probability user $u$ in cluster $k$\\
	 $\mu_k$, $\Sigma_k$ & mean, variance matrix of cluster $k$\\
	 $I_{max}$& number of maximum iterations\\
	\hline
\end{tabular}
\end{center}
\end{table}%

$\quad$

We summary the notations used in this paper in Table \ref{tab}.

\subsection{Model Inference}
\label{modelinference}
Similar to \cite{YangTC16Multi}, we employ an alternating optimization approach for inference. First, we fix the embeddings (i.e., $\{x_u\}$) to update GMM parameters (i.e., $\{\gamma_k\}$, $\{\mu_k\}$, and $\{\Sigma_k\}$):
\begin{equation}
\gamma_{k} = \frac{N_k}{N}, \mu_{k} = \frac{\sum_u r_{uk}x_{u}}{N_k},
\label{eq:gmm1}
\end{equation}

\begin{equation}
\Sigma_{k} = \frac{\sum_u r_{uk}(x_{u}-\mu_k)(x_{u}-\mu_k)^T}{N_k},
\label{eq:gmm2}
\end{equation}
where $r_{uk} \triangleq P(z_u=k|x_u)=\frac{\gamma_{k}\mathcal{N}(x_u|\mu_k,\Sigma_k)}{\sum_{k'}\gamma_{k'}\mathcal{N}(x_i|\mu_{k'},\Sigma_{k'})}$ and $N_k \triangleq \sum_u r_{uk}$. The community membership for each user $u$ can be obtained by MAP estimation,
\begin{equation}
\hat z_u = arg\max_{k} r_{uk} = arg\max_{k} P(z_u=k|x_u).
\label{eq:comm}
\end{equation}


Then, we fix the GMM parameters and update the embeddings via a generalized EM method to fit the survival inverse-Gaussian timestamps in the observation window. In the E-step, we calculate the conditional expectations to impute ``missing'' timestamps $\{\overline{t_v^c}\}$ according to \cite{Whitmore1983A},
\begin{equation}
\widetilde{t_u^c} = E[\overline{t_v^c}|\overline{t_v^c} > T] = d_{uv} \cdot \frac{H(d_{uv}^2/T,d_{uv})}{1-H(T,d_{uv})},
\label{miss}
\end{equation}
where $u$ is the source user of cascade $c$ and $H$ is the distribution function of the inverse-Gaussian distribution.

In the M-step, we first write down the expected complete data log-likelihood function to optimize,
\begin{equation}
\begin{aligned}
\mathcal L & = \sum_{u = 1} ^{N} [\sum_{c \in \mathcal{C}_u}(\sum_{v\in \mathcal{U}_c}  \log f(t_v^c) + \sum_{v\in \overline{\mathcal{U}_c}}\log f(\widetilde{t_v^c})) \\
 &\quad\quad\quad+ \log\sum_{k=1}^{K} \gamma_{uk}\mathcal{N}(x_u;\mu_k,\Sigma_k)]\\
 & \geq  \sum_{u = 1} ^{N} [\sum_{c \in \mathcal{C}_u}(\sum_{v\in \mathcal{U}_c}  \log f(t_v^c) + \sum_{v\in \overline{\mathcal{U}_c}}\log f(\widetilde{t_v^c}))\\
 &\quad\quad\quad+ \sum_{k=1}^{K}\gamma_{uk}\log\mathcal{N}(x_u;\mu_k,\Sigma_k)] = \mathcal{L'}
\end{aligned}
\end{equation}
where the inequality is obtained thanks to log-concavity, i.e., $\log\sum_{k=1}^{K}r_{uk} \mathcal{N}(x_u;\mu_k,\Sigma_k) \geq \sum_{k=1}^{K}r_{uk} \log \mathcal{N}(x_u;\mu_k,\Sigma_k)$. 
We then employ gradient ascent to maximize the lower-bound $\mathcal L'$ of the log-likelihood by updating the embeddings $\{x_u\}$. The gradients are computed as,
\begin{equation}
\begin{aligned}
\frac{\partial \mathcal{L'}}{\partial x_u} &= \sum_{c\in \mathcal C_u}\sum_{v \in \mathcal U_c}(\frac{d_{uv}-t_v^c}{t_v^c} - \frac{1}{d_{uv}})\cdot(x_v-x_u)\\
&+ \sum_{v=1}^N\sum_{c\in \mathcal C_v | u\in \mathcal U_c}(\frac{d_{vu}-t_u^c}{t_v^c} - \frac{1}{d_{vu}})\cdot(x_u-x_v)\\
&+\sum_{c\in \mathcal C_u}\sum_{v\in\overline{\mathcal U_c}}(\frac{d_{uv}-\widetilde{t_v^c}}{\widetilde{t_v^c}} - \frac{1}{d_{uv}})\cdot (x_v-x_u) \\
&+ \sum_{v=1}^N\sum_{c\in \mathcal C_v | u\in \overline{\mathcal U_c}}(\frac{d_{vu}-\widetilde{t_u^c}}{\widetilde{t_u^c}} - \frac{1}{d_{uv}})\cdot(x_u-x_v) \\
&+ \sum_{k=1}^{K}r_{uk}\Sigma_{k}^{-1}(\mu_u - x_u),
\end{aligned}
\label{grad}
\end{equation}
where $d_{uv}=d_{vu}\triangleq \Vert x_u - x_v\Vert^2$.
%and $\mathcal{L}'$ is a lower bound of the log-likelihood $\mathcal L$ thanks to log-concavity, i.e., $\sum_{k=1}^{K}r_{uk} \log \mathcal{N}(\mu_k,\Sigma_k) \leq \log\sum_{k=1}^{K}r_{uk} \mathcal{N}(\mu_k,\Sigma_k)$. 

We briefly explain the intuition behind $x_u$ update. It is influenced by both the difference between the distance and the expected global transmission time (the first four terms) and the consistency with the centroids of communities user $u$ likely belongs to (the last term). The former is weighted by the inverse of transmission time. Thus, we emphasize smaller global transmission time intervals that would contain more information for preserving local network structures, and de-emphasize larger ones that are more noisy and unstable. We refer interested readers to \cite{Luo2011Cauchy} for a similar idea of network embedding. Besides, short distances are penalized by the term $-\frac{1}{d_{uv}}$ to avoid over-fitting. 

Here, dealing with non-infected users is the most time-consuming part (Eq.~(\ref{miss}) and the third to forth lines of Eq.~(\ref{grad})) due to sparsity of diffusion cascades. This computational burden can be reduced by the idea of negative sampling~\cite{mikolov2013distributed}. In our case, we sample a non-infected user set $\overline{\mathcal U_c}$ twice\footnote{There is clearly a trade-off between accuracy and computational cost when determining the ratio of negative samples to positive samples. We here empirically choose ``twice''.} as large as that of the infected user set $\mathcal U_c$ for each cascade $c$. 
% To make it unbiased, the corresponding part of the gradient then should be multiplied by a factor $\beta_{c} = \frac{N - \vert U_c \vert}{\vert \overline{\mathcal U_c}\vert}$.

We iteratively repeat the procedures above for a given number of iterations until convergence as shown in Algorithm~\ref{inference}.

\begin{algorithm}[t]
  \caption{Model Inference}
  \label{inference}
\begin{algorithmic}[1]
\Require 
Diffusion cascades $\cal C$, number of users $N$, number of communities $K$, number of dimensions $D$, learning rate $\alpha$, maximum iterations $I_{max}$.
\Ensure
Embeddings $\{x_u\}$, community memberships $\{\hat z_u\}$, GMM parameters $\{\gamma_k, \mu_k,\Sigma_k\}$

\State Randomly initialize $\{x_u\}$
\For{$i \leftarrow 1$ to $I_{max}$}
  \For{$k \leftarrow 1$ to $K$}
    \State Calculate $\gamma_k, \mu_k,\Sigma_k$ according to Eq.~(\ref{eq:gmm1}-\ref{eq:gmm2})
  \EndFor
  \ForAll{$c \in \mathcal C$}
    \State Sample non-infected users  $\ \overline{\mathcal U_c}$
    \ForAll{$u \in \overline{\mathcal U_c}$}
     \State Impute $\widetilde{t_u^c}$ according to Eq.~(\ref{miss})
    \EndFor
  \EndFor
  \For{$u \leftarrow 1$ to $N$}
    \State Update $x_u$ by gradient ascent according to Eq.~(\ref{grad})
  \EndFor
\EndFor
  \For{$u \leftarrow 1$ to $N$}
    \State Calculate $\hat z_u$ according to Eq.~(\ref{eq:comm})
  \EndFor
\end{algorithmic}
\end{algorithm}

\subsection{Time Complexity Analysis}
In each iteration, we first update GMM parameters with complexity $O(KN)$. The E-step takes $O(\sum_c\vert\mathcal{U}_c\vert)=O(\vert\mathcal{C}\vert)$ to impute timestamps of sampled non-infected users (twice as many as the infected users in our case), while the following M-step takes $O(\vert \mathcal{C} \vert+KN)$ to update embeddings. Here, we use $\vert\mathcal{C}\vert$ to denote the size of input cascades, i.e., the number of all tuples contained in each cascade in $\mathcal{C}$. Overall, the complexity is $O(I_{max}(\vert \mathcal{C} \vert+KN))$, where $I_{max}$ is the number of maximum iterations. 

Therefore, when $K$ and $I_{max}$ are regarded as given constants, our inference algorithm scales linearly in terms of the size of the input data, i.e., the number of users and the size of diffusion cascades.




\section{Experiments}
In this section, we perform various experiments on both synthetic and real-world datasets to evaluate our proposed approach and conduct a case study of online social media sites crawled by Memetracker. We compare COSINE with the following state-of-art baseline methods:

\textbf{C-Rate, C-IC}~\cite{barbieri2013influence} leverage diffusion cascades to discover latent communities in a network-oblivious setting. 

\textbf{CDK}~\cite{bourigault2014learning} is a ranking-based network embedding method to represent users in such a latent vector space that information diffusion can be regarded as a heat diffusion process in that space.

\textbf{NetRate}~\cite{gomez11netrate} is a network inference method for estimating information transmission rates between users based on cascade data. 

\textbf{Pipeline} is the pipelined variant of our model. We first estimate temporal infectivity between users, and then learn network embeddings similar to COSINE.

% We have also planned to compare with the recent method~\cite{bourigault2016representation}. Unfortunately, it does not scale well in our case. Our Tensorflow implementation fails to give a satisfactory result in more than 12 hours in the synthetic dataset. 


\subsection{Synthetic Dataset}
\subsubsection{Data Generation.} 
As in \cite{barbieri2013influence}, we first generate four networks with known community structures by the widely used benchmark for community detection~\cite{PMID:18999496}.  The process of network generation is controlled by the following parameters: (i) number of nodes (1,500), (ii) average degree (10), (iii) maximum degree (200), and (iv) min/max community sizes (50/400). The four networks differ in the mixing parameters $\lambda$ that control the fraction of edges of a node that go outside its community, ranging in  [0.05, 0.1, 0.15, 0.2]. 

Then, we simulate information diffusion processes to generate synthetic diffusion cascades with an observation window $T = 4.0$. For each edge, the local transmission time intervals are sampled from a exponential distribution, while the transmission rate is sampled from a Gamma distribution with the shape parameter $2$ and scale parameter $0.025$. 

\subsubsection{Community Detection.}
Since the ground truth of communities are available in synthetic datasets, we directly compare COSINE's performance on community detection with other baseline methods using NMI (normalized mutual information) as the evaluation metric. We exploit GMM to obtain clusters as communities for CDK and Pipeline, and use the Metis package\footnote{http://glaros.dtc.umn.edu/gkhome/metis/metis/overview} \cite{karypis1998fast} to detect communities from the networks inferred by NetRate. Figure~\ref{fig:nmi} shows that COSINE substantially outperform other baselines. Without the mutual regularization between social infectivity and community-preserving network embeddings, the pipelined approach exhibits worse performance than COSINE especially as the mixing parameter increases (i.e., community structures are less clear). 

\begin{figure}[t]
\includegraphics[scale = 0.38]{nmi}
\caption{Community detection performance with different mixing parameters $\lambda = 0.05$, $0.1$, $0.15$, $0.2$.}
\label{fig:nmi}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[scale = 0.43]{dim}
\caption{Sensitivity w.r.t. different numbers of dimensions.}
\label{fig:dim}
\end{figure}

Figure~\ref{fig:vis} compares the visualization of different embedding approaches. We can see that COSINE not only exhibits better accuracy, but also guarantees more distinct boundaries between different communities. 

\begin{figure*}[t]
% \setlength{\belowcaptionskip}{0.3cm}
% \setlength{\abovecaptionskip}{0.0cm}
\centering
\includegraphics[scale = 0.53]{vis2}
\caption{Visualization of the synthetic networks with mixing parameter $\lambda = 0.15$. Nodes are projected into the 2-D space using t-SNE (Maaten and Hinton, 2008) with the learned embeddings as input. Color of a node indicates the community membership.}
\label{fig:vis}
\end{figure*}

\subsubsection{Diffusion Prediction.}
We also evaluation our model in the task of diffusion prediction. We independently generate a set of test cascades and predict whether users are infected in each cascade in a given time window $T'$ (here, we choose $T' = 1.5$). Because CDK only scores the likelihood of contagion by users' distance from the source user without a prediction threshold, we use the averaged AUC as the evaluation metric for a fair comparison. As shown in Table~\ref{tab:auc}, COSINE demonstrates significant improvement than baseline methods. Community regularization enables COSINE and C-Rate to perform relatively better as the mixing parameter increases, while it is the opposite for other baselines that fail to consider community structures in networks due to over-fitting issues.

\begin{table}[t]
% \setlength{\belowcaptionskip}{-0.1cm}
% \setlength{\abovecaptionskip}{0.0cm}
\caption{Diffusion prediction performance (AUC) on synthetic datasets.}
\begin{center}
\begin{tabular}{c|ccccc}
\hline
\hline
$\lambda$&C-Rate &CDK &NetRate&Pipeline&COSINE\\
\hline
0.05&0.6883&0.8093&0.7877&0.7846&$\mathbf{0.8219^*}$\\
0.10&0.6784&0.8132&0.7778&0.7882&$\mathbf{0.8347^\dagger}$\\
0.15&0.7001&0.8114&0.7687&0.7717&$\mathbf{0.8425^\ddagger}$\\
0.20&0.7025&0.7989&0.7560&0.7636&$\mathbf{0.8378^\ddagger}$\\
\hline
\hline
\end{tabular}
$\quad$

Significantly outperform the best baseline method:

$*$($p<0.05$), $\dagger$($p<0.01$), $\ddagger$($p<0.005$)
\end{center}
\label{tab:auc}
\end{table}%

\subsubsection{Parameter Sensitivity.}
We also demonstrate the sensitivity w.r.t. the numbers of parameter dimensions $D$. As in Figure~\ref{fig:dim}, larger dimensions brings better results until reaching saturation at around 50. Therefore, we set $D = 50$ in the other experiments when not otherwise specified. 

\subsection{Real-world Dataset}

%\textbf{Data description.} 
We use the Memetracker dataset ~\cite{Leskovec2009Meme} which tracks the diffusion of popular quotes and phrases, called ``memes'', across online media sites and blogs from August 2008 to April 2009. We extract the most active 1,000 media sites as ``users'' with 6,000 cascades that are split into halves for training and testing.

\begin{figure}[t]
% \setlength{\belowcaptionskip}{-0.cm}
% \setlength{\abovecaptionskip}{0.2cm}
\centering
\includegraphics[scale = 0.388]{auc_vs_cas2}
\caption{(a) Diffusion prediction performance on Memetracker with different numbers of training cascades. We omit inapplicable or uncompetitive (AUC $<$ 0.8) baselines for better presentation. (b) Running time versus numbers of training cascades.}
\label{fig:auc_cas}
\end{figure}

%\textbf{Diffusion prediction.} 
We investigate the predictive performance with different numbers of training cascades. As illustrated in Figure~\ref{fig:auc_cas}(a), COSINE demonstrates consistent improvement over other baseline methods especially when the number of training cascades is less than 2,000. This result suggests robustness of our model in the setting of sparse data as is often the case in social network analysis.

\subsubsection{Case Study.} 
\begin{figure}[t]
% \setlength{\belowcaptionskip}{.3 cm}
% \setlength{\abovecaptionskip}{0.cm}
\centering
\includegraphics[scale = 0.5]{memetracker}
\caption{Visualization of Memetracker with COSINE.}
\label{fig:meme}
\end{figure}

Figure~\ref{fig:meme} visualizes communities of social media sites detected by COSINE with recognized community themes. Their relative positions and densities provide rich information on proximity relationships both at the individual level and the community level. For example, the communities of French sites are most ``exclusive'' with almost no interaction with other communities, while some individual political sites are embedded in the three communities of mainstream news in the middle or in the community of entertainment to its left, exhibiting different media styles. 

\begin{table*}[t]
% \setlength{\belowcaptionskip}{0.3cm}
%\setlength{\abovecaptionskip}{cm}
\caption{Four illustrative communities in Memetracker with typical domains and sample memes.}
\begin{center}
\begin{tabular}{c|c|c|c}
\hline
\#1: Technology&\#2: Entertainment &\#3: Spanish sites& \#4: German sites\\
\hline
%\multirow{4}{*}{Domains}
computerworld.com &accesshollywood.com &madridiario.es &fr-online.de\\
tech.originalsignal.com &hollywoodreporter.com &abc.es &ad-hoc-news.de \\
dailytech.com &entertainmentwise.com &informador.com.mx &stern.de\\
technewsworld.com &celebrity-gossip.net  &caracol.com.co  &handelsblatt.com\\
\hline
%\multirow{3}{*}{Memes}
 virtual data center os& are you that guy from x-men &atenci n especial& wir sind sehr gl cklich \\
so what's great about 3g&  i'm cool with the paparazzi &la asignatura pendiente &eroberer von davos\\
solar power plant &luckiest girl in the world (song) &noche de estrellas & alles ist m glich\\
\hline
\end{tabular}
\end{center}
\label{tab:case}
\end{table*}%


We also list some typical domains and sample memes of four representative communities in Table~\ref{tab:case}. The community of Spanish sites contains top-level domains of multiple Spanish-speaking countries (e.g., Spain \textit{.es}, Mexico \textit{.mx} and Colombia \textit{.co}) with less closely knitted representations in the 2-D space than its counterparts, the communities of French sites and German sites.



\subsubsection{Running Time.}
For the sake of comparison with all baseline methods, we only focus on relatively small datasets in experiments. Yet, our approach guarantees linear scalability with regard to increasing input data. Figure~\ref{fig:auc_cas}(b) plots the running time of COSINE versus the numbers of training cascades. The experiments are run on Intel(R) Xeon(R) E5-2620@2.00GHz with 64GB of RAM.
\section{Related Work}
\subsubsection{Network Embedding.}
Network embedding recently attracts extensive research interests. In addition to such word2vec-based methods as DeepWalk~\cite{perozzi2014deepwalk}, LINE~\cite{tang2015line}, GraRep~\cite{Cao2015GraRep} and node2vec~\cite{grover2016node2vec}, Planetoid~\cite{Yang2016Revisiting} exploits label information in a semi-supervised manner. Wang et. al.~\shortcite{Wang2016Community} propose a matrix factorization model to preserve community structures in network embeddings. 
However, all these approaches rely on prior knowledge of network structures that are hard to obtain due to technical or privacy issues in many cases.

CDK~\cite{bourigault2014learning} offers a clever solution of learning network embeddings from information diffusion cascades, but the ranking-based approach does not fully exploit temporal information. Later models that capture temporal dynamics of information contagion \cite{kurashima2014probabilistic,bourigault2016representation} focus on the local transmission rates that are computationally difficult to infer and prone to over-fitting issues.

To the best of our knowledge, there is no existing work leveraging community structures in social networks in this line of research. However, we consider it important to take the notion of community into consideration: first, community structures prove to be a pervasive phenomenon both empirically and theoretically in network science; second, as mentioned in the introduction, clustering properties in the embedding space prevent over-fitting which is relevant (though, not mathematically strictly equivalent) to low-rank regularization of the user interaction matrix~\cite{Zhou2013Learning}.

\subsubsection{Community Detection.} 
Our work is also inspired by those community detection methods. 
Newman and Girvan ~\shortcite{Girvan2002Community} are among the first to propose that there exist community structures in social networks. After that, community detection becomes a fruitful research area \cite{papadopoulos2012community} in network science and data mining communities. 

Recent work~\cite{KozdobaM15,Yang2016Modularity} attempts to detect communities via learning network embeddings. Barbieri et. al.~\shortcite{barbieri2013influence} first propose to detect communities from diffusion cascades without networks. NetCodec~\cite{Long2015NetCodec} jointly detects community structure and network infectivity from cascade data. 

\subsubsection{Information Diffusion.} 
Information diffusion is also a vast research domain, attracting extensive research interests \cite{guille2013information} with two types of classic information propagation models, Independent Cascade (IC) Model \cite{kempe2003maximizing} and Linear Threshold (LT) Model \cite{10.2307/2778111}.

Gomez-Rodriguez et. al. propose NETINF \cite{gomez2010inferring} to infer influence strengths between news media sites and blogs from citation cascades. NetRate \cite{gomez11netrate} extends the discrete IC model to a continuous IC model and estimates transmission rates between users from cascading data. Later, there are both empirical and theoretical discussions (e.g., \cite{Du2013Scalable,pmlr-v32-daneshmand14}) on diffusion network recovery from cascades based on the continuous diffusion model, that is also the one we adopt in this paper.


\section{Conclusion}
In this paper, we propose a COmmunity-preserving SocIal Network Embedding (COSINE) model. 
COSINE exploits the information diffusion process to preserve local community structures and reflects rich types of proximity relationships without prior information on network structures.
This model can be applied in various tasks such as community detection, information diffusion prediction and visualization.
Experimental studies are conducted on both synthetic and real-world datasets, demonstrating its effectiveness, robustness and scalability. Our case study shows that COSINE can help us better understand social contagion and interaction relationships in social networks.  

There are various ways to extend our work in the future. One of them is to incorporate semantic information into our social embeddings. We also plan to learn dynamic representations to capture time-varying social networks with evolving community structures.


\section{Acknowledgments}
We would thank Zhiqiang Liu for part of the code implementation.
We would also like to acknowledge the anonymous reviewers for their valuable comments.
This  work  is  supported  by  973  Program  under  Grant  No. 2014CB340405,  NSFC  under  Grant  No.  61532001  and  No.61370054, and MOE-RCOE under Grant No. 2016ZD201.

\bibliographystyle{aaai}
\bibliography{aaai_new}

\end{document}
